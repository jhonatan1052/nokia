class Test(object):
# Author :  Jhonatan Acelas Arevalo
# Date   :  23 / 03 / 2019
    def increasing(self, number):

        counter = 0

        for char in range(0,len(number)-1):
            if  int(  number[char] ) <= int( number[char+1] ) :
                counter = counter +1
            else : 
                break
        
        if counter == len(number)-1:
            return True 
        else :
            return False 

    def decreasing( self, number):
        counter = 0

        for char in range(0,len(number)-1):
            if  int(  number[char] ) >= int( number[char+1] ) :
                counter = counter +1
            else :
                break
        
        if counter == len(number)-1:
            return True 
        else :
            return False 


    def calculate(self):

        a = input('percentage of bouncy numbers you want:')

        bouncy = False 
        number = 0
        increasing_number = 0 
        decreasing_number = 0 

        while  bouncy == False:
        #while  number < 21780:
            number = number + 1
            number_string = str(number)

            if self.increasing(number_string) :
                increasing_number  = increasing_number +1 
            elif  self.decreasing(number_string): 
                decreasing_number = decreasing_number + 1

            print(  ' number : ' + number_string )

            if  ( increasing_number + decreasing_number )/ number  == round( 1 - (float(a)/100) , 3 ) :
                bouncy = True
                break
        print( str( number  ) + ' is the least number for which the proportion of bouncy numbers is exactly   : ' + str ( a ) + '%' ) 

t = Test()
t.calculate()